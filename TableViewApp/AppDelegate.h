//
//  AppDelegate.h
//  TableViewApp
//
//  Created by Christian Mansch on 15.01.14.
//  Copyright (c) 2014 Christian Mansch. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
