//
//  TabellenViewController.m
//  TableViewApp
//
//  Created by Christian Mansch on 15.01.14.
//  Copyright (c) 2014 Christian Mansch. All rights reserved.
//

#import "TabellenViewController.h"
#import "DetailViewController.h"

@interface TabellenViewController ()<UIAlertViewDelegate>
{
    NSMutableString *myText;
}
@property (nonatomic, strong) NSMutableArray *dataArray;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *buttonSort;
- (IBAction)sortCountries:(id)sender;

@end

@implementation TabellenViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    myText = [[NSMutableString alloc] init];
    
    self.navigationItem.rightBarButtonItem = self.editButtonItem;
    
    self.dataArray = [NSMutableArray arrayWithObjects:@"Deutschland", @"Österreich", @"Schweiz", @"Spanien", @"Dänemark", @"Finnland", @"Italien", nil];
 
    
    //Um Umlaute auf der Konsle auszugeben, folgende Notation verwenden.
    
    //NSString *s = [NSString stringWithUTF8String:"Österreich"];
    //NSLog(@"%@",s);
    
    /*UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Test" message:@"This is a test" delegate:nil cancelButtonTitle:@"Cancel" otherButtonTitles:nil];
    
    [alert show];
    
    NSLog(@"After Alert");
     */
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return [self.dataArray count]+1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    // Configure the cell...
    if(indexPath.row == [self.dataArray count])
    {
        [cell setAccessoryType:UITableViewCellAccessoryNone];
    }
    else
        cell.textLabel.text = [self.dataArray objectAtIndex:indexPath.row];
    
    return cell;
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if([segue.identifier isEqualToString:@"detailSegue"])
    {
        DetailViewController *dvc = segue.destinationViewController;
        
        NSIndexPath *indexPath;
        indexPath = [self.tableView indexPathForSelectedRow];
        dvc.auswahl = [self.dataArray objectAtIndex:indexPath.row];
        
    }
}

// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}


// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        [self.dataArray removeObjectAtIndex:indexPath.row];
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert)
    {
        [self showAlertView];
        
        //[self.dataArray addObject:[alert textFieldAtIndex:0].text];
        
        NSLog(@"Hallo");
        //[self.dataArray addObject:@"test"];
        //[tableView insertRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
        
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        
    }
}

-(void)showAlertView
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Eingabe" message:@"Bitte geben Sie ein Land ein" delegate:self cancelButtonTitle:@"Abbrechen" otherButtonTitles:@"OK", nil];
        [alert setAlertViewStyle:UIAlertViewStylePlainTextInput];

        [alert show];
}

-(UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.row == [self.dataArray count])
    {
        return UITableViewCellEditingStyleInsert;
    }
    else
    {
        return UITableViewCellEditingStyleDelete;
    }
}




// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
    NSLog(@"Moved from row :%d, to row:%d", fromIndexPath.row, toIndexPath.row);
    
    if (fromIndexPath.row < toIndexPath.row)
    {
        [self.dataArray insertObject:[self.dataArray objectAtIndex:fromIndexPath.row] atIndex:toIndexPath.row+1];
        [self.dataArray removeObjectAtIndex:fromIndexPath.row];
    
    } else
    {
        [self.dataArray insertObject:[self.dataArray objectAtIndex:fromIndexPath.row] atIndex:toIndexPath.row];
        [self.dataArray removeObjectAtIndex:fromIndexPath.row+1];
    }
    
    NSLog(@"Array: %@", self.dataArray);
}


// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}

#pragma mark - Own Methods

- (IBAction)sortCountries:(id)sender
{
    NSArray *helperArray = [self.dataArray sortedArrayUsingSelector:@selector(compare:)];
    
    [self.dataArray removeAllObjects];
    [self.dataArray addObjectsFromArray:helperArray];
    
    [self.tableView reloadData];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    UITextField *textField = [alertView textFieldAtIndex:0];
    [self.dataArray addObject:textField.text];
    NSLog(@"TextField.text: %@", textField.text);
    [alertView textFieldAtIndex:0].text = textField.text;
}

@end
